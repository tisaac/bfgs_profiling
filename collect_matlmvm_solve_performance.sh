#!/bin/bash

for ARGUMENT in "$@"
do
    KEY=$(echo $ARGUMENT | cut -f1 -d=)
    KEY_LENGTH=${#KEY}
    VALUE="${ARGUMENT:$KEY_LENGTH+1}"

    export "$KEY"="$VALUE"
done

#Inputs: location={home, device}, method={cdbfgs,bfgs}, type={inplace,reorder}, dim=int, hist=int, epochs=int, iters=int directory=string

log_file="${directory}/solve_performance_${location}_${method}_${type}_dim_${dim}_hist_${hist}_epochs_${epochs}_iters_${iters}_np_${NP}.log"

extra_args="-mat_lmvm_hist_size $hist -root_device_context_stream_type default_blocking -log_view :${log_file} -n ${dim} -epochs ${epochs} -iters ${iters}"

if [ $location == "device" ]; then
  extra_args="$extra_args -vec_type cuda -random_type curand"
else
  extra_args="$extra_args -vec_type standard -random_type random123"
fi

if [ $method == "cdbfgs" ]; then
  extra_args="$extra_args -mat_type lmvmcdbfgs"
  if [ $type == "inplace" ]; then
    extra_args="$extra_args -mat_lbfgs_type cd_inplace"
  elif [ $type == "reorder" ]; then
    extra_args="$extra_args -mat_lbfgs_type cd_reorder"
  fi
else
  extra_args="$extra_args -mat_type lmvmbfgs -mat_lmvm_scale_type none"
fi

PETSC_ARCH=${PETSC_ARCH} make -C ${PETSC_DIR} test search='ksp_ksp_utils_lmvm_tests-solve_performance_0' EXTRA_OPTIONS="$extra_args" TIMEOUT=100000000 NP="${NP}"
echo "bfgs_profiling PETSc commit: $(cd $PETSC_DIR && git describe --always --dirty)" >> $log_file

