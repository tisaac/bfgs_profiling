#!/bin/bash



#bash gen_res.sh method=bfgs  dim=1000 hist=5 sync=async

hist=(5 10 20 50)
dim=(10 100 1000 10000 100000 1000000 10000000)
method=("bfgs" "cdbfgs")
sync=("sync" "async")
type=("inplace" "reorder")

for h in "${hist[@]}"
  do
    for d in "${dim[@]}" 
    do
      for s in "${sync[@]}"
      do
        bash gen_res.sh "method=cdbfgs" "dim=$d" "hist=$h" "type=inplace" "sync=$s"	  
        bash gen_res.sh "method=cdbfgs" "dim=$d" "hist=$h" "type=reorder" "sync=$s"	  
        bash gen_res.sh "method=bfgs" "dim=$d" "hist=$h" "sync=$s"	  
    done
  done
done

