#!/bin/bash

for ARGUMENT in "$@"
do
    KEY=$(echo $ARGUMENT | cut -f1 -d=)
    KEY_LENGTH=${#KEY}
    VALUE="${ARGUMENT:$KEY_LENGTH+1}"

    export "$KEY"="$VALUE"
done

#bash directory=path PETSC_DIR=path

location=("host" "device")
hist=(5 10 20 50)
dim=(100 1000 10000 100000 1000000 10000000)
method=("bfgs" "cdbfgs")
type=("inplace" "reorder")
epochs=1

for h in "${hist[@]}"
do
  for d in "${dim[@]}"
  do
    for loc in "${location[@]}"
    do
      if [ $d -eq 10000000 ]; then
        iters=25
      else
        iters=100
      fi
      bash ./collect_matlmvm_solve_performance.sh "NP=1" "PETSC_DIR=${PETSC_DIR}" "PETSC_ARCH=${PETSC_ARCH}" "directory=${directory}" "location=${loc}" "epochs=${epochs}" "iters=${iters}" "dim=${d}" "hist=${h}" "method=cdbfgs" "type=inplace"
      bash ./collect_matlmvm_solve_performance.sh "NP=1" "PETSC_DIR=${PETSC_DIR}" "PETSC_ARCH=${PETSC_ARCH}" "directory=${directory}" "location=${loc}" "epochs=${epochs}" "iters=${iters}" "dim=${d}" "hist=${h}" "method=cdbfgs" "type=reorder"
      bash ./collect_matlmvm_solve_performance.sh "NP=1" "PETSC_DIR=${PETSC_DIR}" "PETSC_ARCH=${PETSC_ARCH}" "directory=${directory}" "location=${loc}" "epochs=${epochs}" "iters=${iters}" "dim=${d}" "hist=${h}" "method=bfgs" "type=reorder"
    done
  done
done

